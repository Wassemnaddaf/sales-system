package com.quiz.salesSystem.service;

import com.quiz.salesSystem.dao.ClientDao;
import com.quiz.salesSystem.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    ClientDao clientDao;

    public List<Client> fetchClients() {
        try {
            return clientDao.fetchClients();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Client createNewClient(Client client) {
        try {
            return clientDao.save(client);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Client updateClient(Client client) {
        try {
            return clientDao.save(client);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
