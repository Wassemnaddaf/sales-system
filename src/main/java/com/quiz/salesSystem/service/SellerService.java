package com.quiz.salesSystem.service;

import com.quiz.salesSystem.dao.SellerDao;
import com.quiz.salesSystem.entity.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SellerService {
    @Autowired
    SellerDao sellerDao;

    public List<Seller> fetchSellers() {
        try {
            return sellerDao.findAll();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Seller createNewSeller(Seller seller) {
        try {
            return sellerDao.save(seller);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Seller updateSeller(Seller seller) {
        try {
            return sellerDao.save(seller);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
