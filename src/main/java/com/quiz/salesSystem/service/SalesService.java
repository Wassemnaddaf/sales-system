package com.quiz.salesSystem.service;

import com.quiz.salesSystem.aspect.Log;
import com.quiz.salesSystem.dao.ProductSalesDao;
import com.quiz.salesSystem.dao.SalesDao;
import com.quiz.salesSystem.entity.ProductSales;
import com.quiz.salesSystem.entity.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class SalesService {

    @Autowired
    SalesDao salesDao;
    @Autowired
    ProductSalesDao productSalesDao;

    public List<Sales> fetchSales() {
        try {
            return salesDao.fetchSales();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Sales createNewSales(Sales sales) {
        try {
            Set<ProductSales> productSales = sales.getProductSales();
            for (ProductSales p : productSales)
                p.setSales(sales);
            return salesDao.save(sales);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Log
    public ProductSales updateSale(ProductSales sales) {
        try {
            return productSalesDao.save(sales);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
