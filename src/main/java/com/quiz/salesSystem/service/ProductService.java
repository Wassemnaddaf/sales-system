package com.quiz.salesSystem.service;

import com.quiz.salesSystem.dao.ProductDao;
import com.quiz.salesSystem.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductDao productDao;

    public Product createNewProduct(Product product) {
        try {
            return productDao.save(product);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<Product> fetchProducts() {
        try {
            return productDao.fetchProducts();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Product updateProduct(Product product) {
        try {
            return productDao.save(product);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
