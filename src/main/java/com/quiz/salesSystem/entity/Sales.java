package com.quiz.salesSystem.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Sales extends GenericEntity{

    private Double total;
    private Client client;
    private Seller seller;
    private Set<ProductSales> productSales;

    @Column(name = "Total")
    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @ManyToOne
    @JoinColumn(name = "Client_Id",referencedColumnName = "Id")
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne
    @JoinColumn(name = "Seller_Id",referencedColumnName = "Id")
    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    @OneToMany(mappedBy = "sales",cascade = CascadeType.ALL)
    @JsonManagedReference
    public Set<ProductSales> getProductSales() {
        return productSales;
    }

    public void setProductSales(Set<ProductSales> productSales) {
        this.productSales = productSales;
    }
}
