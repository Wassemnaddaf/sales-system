package com.quiz.salesSystem.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Product extends GenericEntity {
    private String name;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date creationDate = new Date();

    private ProductCategory productCategory;
    private Set<ProductSales> productSales;


    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "Creation_Date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "Product_category_Id", referencedColumnName = "Id")
    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    @OneToMany(mappedBy = "sales")
    public Set<ProductSales> getProductSales() {
        return productSales;
    }

    public void setProductSales(Set<ProductSales> productSales) {
        this.productSales = productSales;
    }
}
