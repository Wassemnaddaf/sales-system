package com.quiz.salesSystem.aspect;

import com.quiz.salesSystem.dao.LogDao;
import com.quiz.salesSystem.entity.Log;
import com.quiz.salesSystem.entity.ProductSales;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Aspect
@Component
public class LoggingAspect {

    @Autowired
    LogDao logDao;

    @AfterReturning(pointcut="execution(* com.quiz.salesSystem.service.SalesService.updateSale(..))",
            returning="sale")
    public void updateSaleLog(Object sale) {
        ProductSales productSales= (ProductSales) sale;
        Log logData=new Log();
        logData.setLastModifiedDate(new Date());
        logData.setSaleId(productSales.getSales().getId());
        logDao.save(logData);
    }
}
