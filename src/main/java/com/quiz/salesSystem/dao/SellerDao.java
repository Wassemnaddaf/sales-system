package com.quiz.salesSystem.dao;

import com.quiz.salesSystem.entity.Seller;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellerDao extends JpaRepository<Seller,Integer> {
}
