package com.quiz.salesSystem.dao;

import com.quiz.salesSystem.entity.ProductSales;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductSalesDao extends JpaRepository<ProductSales,Integer> {
}
