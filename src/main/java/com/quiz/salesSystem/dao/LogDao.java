package com.quiz.salesSystem.dao;

import com.quiz.salesSystem.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogDao extends JpaRepository<Log,Integer> {
}
