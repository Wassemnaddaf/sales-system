package com.quiz.salesSystem.dao;

import com.quiz.salesSystem.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductDao extends JpaRepository<Product,Integer> {

    @Query("SELECT DISTINCT P FROM Product P " +
            "INNER JOIN FETCH P.productCategory PC ")
    List<Product> fetchProducts();
}
