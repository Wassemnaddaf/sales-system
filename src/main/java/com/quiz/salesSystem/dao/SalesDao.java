package com.quiz.salesSystem.dao;

import com.quiz.salesSystem.entity.Sales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SalesDao extends JpaRepository<Sales,Integer> {

    @Query("SELECT S FROM Sales S " +
            "INNER JOIN FETCH S.productSales PC " +
            "INNER JOIN FETCH S.seller SE " +
            "INNER JOIN FETCH S.client C ")
    List<Sales> fetchSales();
}
