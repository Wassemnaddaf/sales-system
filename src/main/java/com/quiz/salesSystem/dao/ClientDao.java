package com.quiz.salesSystem.dao;

import com.quiz.salesSystem.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClientDao extends JpaRepository<Client,Integer> {

    @Query("SELECT C FROM Client C ")
    List<Client> fetchClients();
}
