package com.quiz.salesSystem.controller;

import com.quiz.salesSystem.entity.Client;
import com.quiz.salesSystem.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClientService clientService;

    @GetMapping
    public List<Client> fetchClients() {
        return clientService.fetchClients();
    }

    @PostMapping
    public Client createNewClient(@RequestBody Client client) {
        return clientService.createNewClient(client);
    }

    @PutMapping
    public Client updateClient(@RequestBody Client client) {
        return clientService.updateClient(client);
    }
}
