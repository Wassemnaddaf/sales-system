package com.quiz.salesSystem.controller;

import com.quiz.salesSystem.entity.ProductSales;
import com.quiz.salesSystem.entity.Sales;
import com.quiz.salesSystem.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    SalesService salesService;

    @GetMapping
    public List<Sales> fetchSales() {
        return salesService.fetchSales();
    }

    @PostMapping
    public Sales createNewSales(@RequestBody Sales Sales) {
        return salesService.createNewSales(Sales);
    }

    @PutMapping
    public ProductSales updateSale(@RequestBody ProductSales productSales) {
        return salesService.updateSale(productSales);
    }
}
