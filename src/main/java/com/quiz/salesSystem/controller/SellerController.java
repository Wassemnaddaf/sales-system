package com.quiz.salesSystem.controller;

import com.quiz.salesSystem.entity.Seller;
import com.quiz.salesSystem.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/seller")
public class SellerController {

    @Autowired
    SellerService sellerService;

    @GetMapping
    public List<Seller> fetchSellers() {
        return sellerService.fetchSellers();
    }

    @PostMapping
    public Seller createNewSeller(@RequestBody Seller seller) {
        return sellerService.createNewSeller(seller);
    }

    @PutMapping
    public Seller updateSeller(@RequestBody Seller seller) {
        return sellerService.updateSeller(seller);
    }
}
