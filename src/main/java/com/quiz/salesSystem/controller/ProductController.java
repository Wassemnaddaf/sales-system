package com.quiz.salesSystem.controller;

import com.quiz.salesSystem.entity.Product;
import com.quiz.salesSystem.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;


    @GetMapping
    public List<Product> fetchProducts() {
        return productService.fetchProducts();
    }

    @PostMapping
    public Product createNewProduct(@RequestBody Product product) {
        return productService.createNewProduct(product);
    }

    @PutMapping
    public Product updateProduct(@RequestBody Product product) {
        return productService.updateProduct(product);
    }
}
