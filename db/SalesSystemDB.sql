USE [master]
GO
/****** Object:  Database [SalesSystemDB]    Script Date: 10/26/2020 10:20:39 PM ******/
CREATE DATABASE [SalesSystemDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SalesSystemDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SalesSystemDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SalesSystemDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SalesSystemDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SalesSystemDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SalesSystemDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SalesSystemDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SalesSystemDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SalesSystemDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SalesSystemDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SalesSystemDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [SalesSystemDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SalesSystemDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SalesSystemDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SalesSystemDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SalesSystemDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SalesSystemDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SalesSystemDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SalesSystemDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SalesSystemDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SalesSystemDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SalesSystemDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SalesSystemDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SalesSystemDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SalesSystemDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SalesSystemDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SalesSystemDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SalesSystemDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SalesSystemDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SalesSystemDB] SET  MULTI_USER 
GO
ALTER DATABASE [SalesSystemDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SalesSystemDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SalesSystemDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SalesSystemDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SalesSystemDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SalesSystemDB]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Last_Name] [varchar](255) NULL,
	[Mobile] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Last_Modified_Date] [datetime2](7) NULL,
	[Sale_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Creation_Date] [datetime2](7) NULL,
	[Description] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[Product_category_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Category_Name] [varchar](255) NULL,
	[Category_Type] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductSales]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSales](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Product_Id] [int] NULL,
	[Sales_Id] [int] NULL,
	[Price] [float] NULL,
	[Quantity] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sales]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sales](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Total] [float] NULL,
	[Client_Id] [int] NULL,
	[Seller_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Seller]    Script Date: 10/26/2020 10:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Seller](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Last_Name] [varchar](255) NULL,
	[Mobile] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Client] ON 

INSERT [dbo].[Client] ([id], [Last_Name], [Mobile], [Name]) VALUES (1, N'Naddaf', N'093202020', N'Wassem')
SET IDENTITY_INSERT [dbo].[Client] OFF
SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([id], [Last_Modified_Date], [Sale_Id]) VALUES (1, CAST(N'2020-10-26 03:55:10.9490000' AS DateTime2), 5)
INSERT [dbo].[Log] ([id], [Last_Modified_Date], [Sale_Id]) VALUES (2, CAST(N'2020-10-26 04:03:59.0860000' AS DateTime2), 5)
INSERT [dbo].[Log] ([id], [Last_Modified_Date], [Sale_Id]) VALUES (3, CAST(N'2020-10-26 22:11:11.0120000' AS DateTime2), 5)
INSERT [dbo].[Log] ([id], [Last_Modified_Date], [Sale_Id]) VALUES (4, CAST(N'2020-10-26 22:11:15.1990000' AS DateTime2), 5)
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([id], [Creation_Date], [Description], [Name], [Product_category_Id]) VALUES (2, CAST(N'2020-10-26 02:30:32.0930000' AS DateTime2), N'128 GB , 8 GB RAM', N'iphone 11', 3)
INSERT [dbo].[Product] ([id], [Creation_Date], [Description], [Name], [Product_category_Id]) VALUES (3, CAST(N'2020-10-26 02:29:42.7090000' AS DateTime2), N'128 GB , 8 GB RAM', N'iphone 10', 2)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([id], [Category_Name], [Category_Type]) VALUES (1, N'APPLE', N'MOBILE')
INSERT [dbo].[ProductCategory] ([id], [Category_Name], [Category_Type]) VALUES (2, N'APPLE', N'MOBILE')
INSERT [dbo].[ProductCategory] ([id], [Category_Name], [Category_Type]) VALUES (3, N'APPLE', N'MOBILE')
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
SET IDENTITY_INSERT [dbo].[ProductSales] ON 

INSERT [dbo].[ProductSales] ([id], [Product_Id], [Sales_Id], [Price], [Quantity]) VALUES (1, 2, NULL, 50000, 2)
INSERT [dbo].[ProductSales] ([id], [Product_Id], [Sales_Id], [Price], [Quantity]) VALUES (2, 2, NULL, 50000, 2)
INSERT [dbo].[ProductSales] ([id], [Product_Id], [Sales_Id], [Price], [Quantity]) VALUES (3, 2, 4, 50000, 2)
INSERT [dbo].[ProductSales] ([id], [Product_Id], [Sales_Id], [Price], [Quantity]) VALUES (4, 2, 5, 500212, 3)
SET IDENTITY_INSERT [dbo].[ProductSales] OFF
SET IDENTITY_INSERT [dbo].[Sales] ON 

INSERT [dbo].[Sales] ([id], [Total], [Client_Id], [Seller_Id]) VALUES (1, NULL, 1, 1)
INSERT [dbo].[Sales] ([id], [Total], [Client_Id], [Seller_Id]) VALUES (2, NULL, 1, 1)
INSERT [dbo].[Sales] ([id], [Total], [Client_Id], [Seller_Id]) VALUES (3, NULL, 1, 1)
INSERT [dbo].[Sales] ([id], [Total], [Client_Id], [Seller_Id]) VALUES (4, NULL, 1, 1)
INSERT [dbo].[Sales] ([id], [Total], [Client_Id], [Seller_Id]) VALUES (5, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[Sales] OFF
SET IDENTITY_INSERT [dbo].[Seller] ON 

INSERT [dbo].[Seller] ([id], [Last_Name], [Mobile], [Name]) VALUES (1, N'EmmaTell', N'092912992', N'EmmaTell')
SET IDENTITY_INSERT [dbo].[Seller] OFF
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FKl8biv8wkkhudr9udn4pldw674] FOREIGN KEY([Product_category_Id])
REFERENCES [dbo].[ProductCategory] ([id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FKl8biv8wkkhudr9udn4pldw674]
GO
ALTER TABLE [dbo].[ProductSales]  WITH CHECK ADD  CONSTRAINT [FK44jg2ra4nbubh3gwlka9tlji3] FOREIGN KEY([Product_Id])
REFERENCES [dbo].[Product] ([id])
GO
ALTER TABLE [dbo].[ProductSales] CHECK CONSTRAINT [FK44jg2ra4nbubh3gwlka9tlji3]
GO
ALTER TABLE [dbo].[ProductSales]  WITH CHECK ADD  CONSTRAINT [FKpbg08e7yuh9byp2dwcsg2a81u] FOREIGN KEY([Sales_Id])
REFERENCES [dbo].[Sales] ([id])
GO
ALTER TABLE [dbo].[ProductSales] CHECK CONSTRAINT [FKpbg08e7yuh9byp2dwcsg2a81u]
GO
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK1kl5ja0ih6fvnhdm2l5fc9dg3] FOREIGN KEY([Seller_Id])
REFERENCES [dbo].[Seller] ([id])
GO
ALTER TABLE [dbo].[Sales] CHECK CONSTRAINT [FK1kl5ja0ih6fvnhdm2l5fc9dg3]
GO
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK4in2pye9auca59o9pb2ot8b6t] FOREIGN KEY([Client_Id])
REFERENCES [dbo].[Client] ([id])
GO
ALTER TABLE [dbo].[Sales] CHECK CONSTRAINT [FK4in2pye9auca59o9pb2ot8b6t]
GO
USE [master]
GO
ALTER DATABASE [SalesSystemDB] SET  READ_WRITE 
GO
